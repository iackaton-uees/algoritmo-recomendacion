import json

# Funcion para extraer las claves respectivas en caso de haber
def extract_key(json_obj, possible_keys):
    for key in possible_keys:
        if key in json_obj:
            return json_obj[key]
    return None

# Funcion para calcular la eficiencia de un contrato
def calculate_efficiency(contract):
    start_date = extract_key(contract, ['awardPeriod', 'startDate'])
    end_date = extract_key(contract, ['awardPeriod', 'endDate'])
    if start_date and end_date:
        efficiency = (end_date - start_date).days
        return efficiency
    return None

# Funcion para calcular el precio de un contrato
def calculate_price(contract):
    return extract_key(contract, ['awards', 0, 'value', 'amount'])

# Funcion para calcular la calidad de un contrato
def calculate_quality(contract):
    description = extract_key(contract, ['description'])
    if description:
        quality_score = min(len(description) / 100, 10.0)
        return quality_score
    else:
        return 0.0

# Funcion para recomendar el mejor contrato
def recommend_best_contract(contracts, criteria='price', weights=None):
    best_contract = None
    best_score = float('inf') if criteria == 'price' else float('-inf')
    
    for contract in contracts:
        if criteria == 'price':
            score = calculate_price(contract)
        elif criteria == 'efficiency':
            score = calculate_efficiency(contract)
        elif criteria == 'quality':
            score = calculate_quality(contract)
        elif criteria == 'combined_score':
            price_score = calculate_price(contract) if weights[0] else 0
            efficiency_score = calculate_efficiency(contract) if weights[1] else 0
            quality_score = calculate_quality(contract) if weights[2] else 0
            if None not in [price_score, efficiency_score, quality_score]:
                score = weights[0] * price_score + weights[1] * efficiency_score + weights[2] * quality_score
            else:
                score = None
        
        if score is not None:
            if criteria == 'price':
                if score < best_score:
                    best_score = score
                    best_contract = contract
            else:
                if score > best_score:
                    best_score = score
                    best_contract = contract
    
    return best_contract, best_score

with open('data.json', 'r') as file:
    contract_data = json.load(file)

weights = (0.4, 0.4, 0.2)  # Precio: 40%, Eficiencia: 40%, Calidad: 20%

# Ejemplo de uso
best_contract_price, best_score_price = recommend_best_contract(contract_data, criteria='price', weights=None)
print('Best contract based on price:', best_contract_price)
print('Score:', best_score_price)

best_contract_efficiency, best_score_efficiency = recommend_best_contract(contract_data, criteria='efficiency', weights=None)
print('Best contract based on efficiency:', best_contract_efficiency)
print('Score:', best_score_efficiency)

best_contract_quality, best_score_quality = recommend_best_contract(contract_data, criteria='quality', weights=None)
print('Best contract based on quality:', best_contract_quality)
print('Score:', best_score_quality)

best_contract_combined, best_score_combined = recommend_best_contract(contract_data, criteria='combined_score', weights=weights)
print('Best contract based on combined score:', best_contract_combined)
print('Score:', best_score_combined)
